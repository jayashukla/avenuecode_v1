Feature: CreateSubtask

	Scenario: Successful display of subtask popup
  Description: This feature is to test the subtask todo functionality
  Given	User is on Todo list page
  When User click on Manage subtasks
  Then Subtask popup should be displayed

  Scenario Outline: Successful creation of subtask.
  Given User is on subtask popup for a todo
  When User provide <Description>  and <Due Date>
  And User clicked on add
  Then Subtask popup should be displayed
  And Subtask <Description> will be <Created or Not>
  Scenarios:
      | Description | Due Date | Created or Not |
      |  subtask1 |   10/09/2017 | Created      |
      |  subtask5 |   0/8/2017   | Not          |
      |           |   10/9/2017  | Not          |
  