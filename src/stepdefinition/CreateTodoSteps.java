package stepdefinition;

import static org.testng.Assert.assertNotEquals;
import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import TodoPage.TodoPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateTodoSteps {
  WebDriver driver = null;
    TodoPage todoPage;
    String baseURL;
    String userName;
    String pwd;
    
	@Before public void setup() {
		driver = new ChromeDriver();
		 baseURL = "http://qa-test.avenuecode.com/";
		 userName = "shuklajaya05@gmail.com";
		 pwd = "avenuetest";
		
		//driver.manage().window().maximize();
		driver.get(baseURL);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		
		todoPage = new TodoPage(driver);
		todoPage.loadHomePage(baseURL);
		todoPage.getLoginPage();
		todoPage.completeLogin(userName, pwd);
		todoPage.getTasksPage();
	}
	
	@Given("^User is on Todo list page$")
	public void user_is_on_Home_Page() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@When("^User entered a ?(.*) todo$")
	public void user_entered_new_todo(String todoName) {
		todoPage.createNewTask(todoName);
	}
	
	@Then("New todo ?(.*) is created$")
	public void new_todo_created(String todoName) {
		int todoLength = todoName.trim().length();
		int MIN_LENGTH = 3;
		int MAX_LENGTH = 250;
		String lastTodo = todoPage.getLastCreatedTodo();
		
		System.out.println("CREATEDDDD TODO" + lastTodo);
		System.out.println("input TODO" + todoName);
		
		if (todoLength == 0) {
			assertNotEquals(lastTodo, "empty");
		} else if(todoLength < MIN_LENGTH) {
			assertNotEquals(lastTodo, todoName.trim());
		} else if (todoLength > MAX_LENGTH) {
			assertNotEquals(lastTodo,todoName.trim());
		} else {
		   assertEquals(lastTodo,todoName.trim());
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	@After public void cleanUp() throws InterruptedException {
		Thread.sleep(1000);
		driver.close();
	}
}