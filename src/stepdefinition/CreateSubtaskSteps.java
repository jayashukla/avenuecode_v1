package stepdefinition;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import TodoPage.SubtaskPage;
import TodoPage.TodoPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateSubtaskSteps {
    WebDriver driver = null;
    TodoPage todoPage;
    SubtaskPage subtaskPage;
    String BASE_URL;
    String userName, pwd;
    
	@Before public void setup() {
		driver = new ChromeDriver();
		BASE_URL = "http://qa-test.avenuecode.com/";
		userName = "shuklajaya05@gmail.com";
		pwd = "avenuetest";
		todoPage = new TodoPage(driver);
		subtaskPage = new SubtaskPage(driver);
		
		todoPage.loadHomePage(BASE_URL);
		todoPage.getLoginPage();
		todoPage.completeLogin(userName, pwd);
		todoPage.getTasksPage();
	}
	
	@Given("^User is on Todo page$")
	public void user_is_on_todo_Page() {
	}
	
	@When("^User click on Manage subtasks$")
	public void user_entered_new_todo() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		subtaskPage.openSubtaskManageDialog();
	}
	
	@Then("Subtask popup should be displayed")
	public void display_subtask_popup() {
		String str = subtaskPage.getDialogCaption();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Validation for read only field
		String pattern = "(.*\\d+)";
		assertTrue(str.matches(pattern));

	    // Validation for description  
	   assertNotNull(subtaskPage.getTextAreaMessage());
	}
	
	@Given("^User is on subtask popup for a todo$")
	public void user_is_on_subtask_popup() {
		WebElement addBtn = driver.findElement(By.xpath("//button[contains(@ng-click,'editModal')]"));
		Actions action = new Actions(driver);
		action.moveToElement(addBtn).click().perform();
	}
	@When("^User provide ?(.*) and ?(.*)$")
	public void user_provide_inputs(String desc, String date) {
		WebElement newTask = driver.findElement(By.id("new_sub_task"));
		newTask.sendKeys(desc);
		
		WebElement taskDate = driver.findElement(By.id("dueDate"));
		taskDate.clear();
		taskDate.sendKeys(date);
	}
	
	@When("User clicked on add")
	public void add_subtask() {
		WebElement addBtn = driver.findElement(By.id("add-subtask"));
		Actions action = new Actions(driver);
		action.moveToElement(addBtn).click().perform();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Then("^Subtask ?(.*) will be ?(.*)$")
	public void validate_subtask(String subTaskName, String res) {
		String lastSubtask = subtaskPage.getLastSubtask();
		System.out.println("last subtask is: " + lastSubtask);
		System.out.println("Request is: " + subTaskName);
		System.out.println("Result is: " + res);
		
		
		if (res.equals("Not")) {
		  assertNotEquals(lastSubtask, subTaskName);
		} else {
		 assertEquals(lastSubtask, subTaskName); 
		}
	}
	
	@After public void cleanUp() throws InterruptedException {
		Thread.sleep(2000);
		driver.close();
	}
}
