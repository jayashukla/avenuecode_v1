package TodoPage;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TodoPage {
	 WebDriver driver = null;
	 By emailField;
	 By pwdField;
	 By goLoginBtn;
	 By loginBtnField;
	 By getTasksBtn;
	 By getNewTaskElement;
	 By addButton;
	 WebElement goBtn;
	 By newTask;
	 By addBtnPath;
	 WebElement addBtn;
	 
	 public TodoPage(WebDriver driver){
	        this.driver = driver;
	        emailField = By.id("user_email");
	   	    pwdField = By.id("user_password");
	   	    goLoginBtn =  By.xpath("//ul[@class='nav navbar-nav navbar-right']//a[@href='/users/sign_in']");
	   	    loginBtnField = By.name("commit");
	   	    getTasksBtn = By.xpath("//div[@class='jumbotron grey-jumbotron']//a[@href='/tasks']");
	     	addBtnPath = By.xpath("//div[@class='task_container ng-scope']//div[@class='well']//span[@class='input-group-addon glyphicon glyphicon-plus']");
	     	newTask = By.id("new_task");
	    }
	 
	 public void loadHomePage(String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 }
	 
	//Set user name and password in textbox
    public void completeLogin(String strUserName, String strPwd){
      driver.findElement(emailField).sendKeys(strUserName);
      driver.findElement(pwdField).sendKeys(strPwd);
      driver.findElement(loginBtnField).click();	
	}	
		
	 public void getLoginPage(){
		 goBtn = driver.findElement(goLoginBtn);
		 Actions action = new Actions(driver);
		 action.moveToElement(goBtn).click().perform();
	 }
	 public void getTasksPage(){
		 goBtn = driver.findElement(getTasksBtn);
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 Actions action = new Actions(driver);
		 action.moveToElement(goBtn).click().perform();
	 }
	 public void createNewTask(String todoName) {
		    driver.findElement(newTask).sendKeys(todoName);
			//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			addBtn =driver.findElement(addBtnPath);
			Actions action = new Actions(driver);
			action.moveToElement(addBtn).click().perform();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 }
	 public String getLastCreatedTodo() {
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 //WebElement todo = driver.findElement(By.xpath("//td[@class='task_body col-md-7']//a"));
		// WebElement todo = driver.findElement(By.xpath("//a[@class='ng-scope ng-binding editable editable-click']"));
		 java.util.List<WebElement> elements = driver.findElements(By.xpath("//td[@class='task_body col-md-7']//a"));
		 int len = elements.size();
	     String str  = elements.get(0).getAttribute("text");
		 return str;
	 }
	
}
