package TodoPage;

import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SubtaskPage {
	 WebDriver driver = null;
	 By addNewTaskBtn = By.id("new_task");
	 By manageDialogPath = By.xpath("//button[contains(@ng-click,'editModal')]");
	 WebElement manageSubtaskBtn;
	 By subTaskLbl = By.xpath("//h3[@class='modal-title ng-binding']");
	 By textAreaLbl = By.xpath("//textarea");
	 By subTaskNameLbl= By.xpath("//td[@class='task_body col-md-8']//a");
	 
	 public SubtaskPage(WebDriver driver){
	        this.driver = driver;
	    }
	 
	 public void loadSubtaskPage() {
		 driver.findElement(manageDialogPath).click();
	 }
	 
	 public void addNewTask(String subtaskName) {
		 driver.findElement(addNewTaskBtn).sendKeys(subtaskName);	
	 }	
	 
	 public void openSubtaskManageDialog() {
		 driver.findElement(manageDialogPath).click();
	 }
	 
	 public String getDialogCaption() {
		String str = driver.findElement(subTaskLbl).getAttribute("innerHTML");	
		return str;
	 }

	 public String getTextAreaMessage() {
		String str = driver.findElement(textAreaLbl).getAttribute("value");	
		return str;
	 }
	 
	 public String getLastSubtask() {
		 java.util.List<WebElement> elements = driver.findElements(subTaskNameLbl);
		 int len = elements.size();
	     String str  = elements.get(len-1).getAttribute("text");	
	     System.out.println("Len is " + len);
		 return str;
	 }
	 
}
